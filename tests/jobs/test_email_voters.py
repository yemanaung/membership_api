from unittest import TestCase
from unittest.mock import Mock, patch

from overrides import overrides

from jobs.email_voters import EmailVoters
from membership.database.base import engine, metadata, Session
from membership.database.models import Election, Member
from tests.io_utils import devnull


class TestDB(TestCase):

    @classmethod
    @overrides
    def setUp(cls):
        metadata.create_all(engine)

    @classmethod
    @overrides
    def tearDown(cls):
        metadata.drop_all(engine)


@patch('membership.util.email.send_member_emails', return_value=None)
class TestEmailVoters(TestDB):
    job = EmailVoters()
    job.confirm_dialog = Mock(return_value=True)

    template = 'membership/templates/upcoming_vote_email.html'

    # Tests

    def test_success(self, send_member_emails):
        session = Session()
        election = Election(
            name="1917 Steering Committee",
            voting_begins_epoch_millis=-1645653600000,
            voting_ends_epoch_millis=-1645642800000)
        session.add(election)
        member = Member()
        session.add(member)
        session.commit()
        self.job.send_eligible_reminder_emails([member], election, self.template, out=devnull)
