import pytest

from membership.database.base import engine, metadata, Session
from membership.services import MeetingService, ValidationError


class TestMeetingService:

    def setup_method(cls, method):
        metadata.create_all(engine)

    def teardown_method(cls, method):
        metadata.drop_all(engine)

    def test_add_chapter_meeting(self):
        session = Session()

        name = 'Chapter Meeting'

        service = MeetingService()
        result = service.add_meeting(name, None, session)
        session.commit()

        assert result.id == 1
        assert result.name == name

    def test_add_committee_meeting(self):
        session = Session()

        name = 'Committee Meeting'
        committee_id = 1

        service = MeetingService()
        result = service.add_meeting(name, committee_id, session)
        session.commit()

        assert result.id == 1
        assert result.name == name
        assert result.committee_id == committee_id

    def test_set_meeting_code(self):
        session = Session()
        service = MeetingService()

        meeting = service.add_meeting('Chapter Meeting', None, session)

        code = 1234
        result = service.set_meeting_code(meeting, code, session)

        assert result.short_id == code

    def test_unset_meeting_code(self):
        session = Session()
        service = MeetingService()

        meeting = service.add_meeting('Chapter Meeting', None, session)

        code = None
        result = service.set_meeting_code(meeting, code, session)

        assert result.short_id == code

    def test_set_meeting_code_invalid(self):
        session = Session()
        service = MeetingService()

        meeting = service.add_meeting('Chapter Meeting', None, session)

        with pytest.raises(ValidationError):
            service.set_meeting_code(meeting, 123, session)

    def test_set_meeting_code_autogenerate(self):
        session = Session()
        service = MeetingService()

        meeting = service.add_meeting('Chapter Meeting', None, session)
        meeting = service.set_meeting_code(meeting, 'autogenerate', session)

        assert 1000 <= meeting.short_id <= 9999

    def test_set_meeting_code_existing_code(self):
        session = Session()
        service = MeetingService()

        short_id = 1234

        meeting = service.add_meeting('Chapter Meeting', None, session)
        meeting = service.set_meeting_code(meeting, short_id, session)
        meeting = service.set_meeting_code(meeting, 'autogenerate', session)

        assert meeting.short_id != short_id
