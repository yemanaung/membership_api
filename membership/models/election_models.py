from transitions import Machine
from typing import List, Optional


class ElectionStateMachine:

    DRAFT = 'draft'
    PUBLISHED = 'published'
    FINAL = 'final'
    CANCELED = 'canceled'
    STATUSES = [
        DRAFT,
        PUBLISHED,
        FINAL,
        CANCELED,
    ]
    INITIAL_STATUS = STATUSES[0]
    TRANSITIONS = [
        {'trigger': 'publish', 'source': DRAFT, 'dest': PUBLISHED},
        {'trigger': 'finalize', 'source': PUBLISHED, 'dest': FINAL},
        {'trigger': 'cancel', 'source': [DRAFT, PUBLISHED], 'dest': CANCELED},
    ]

    def __init__(self, status: Optional[str] = None) -> None:
        self.machine = Machine(
            initial=self._initial_status(status),
            model=self,
            states=ElectionStateMachine.STATUSES,
            transitions=ElectionStateMachine.TRANSITIONS,
            auto_transitions=False,
        )

    def __eq__(self, other) -> bool:
        if type(other) == str:  # for transitions library
            super
        else:
            return self.state == other.state

    @property
    def status(self):
        return self.state

    def next_transitions(self) -> List[str]:
        return self.machine.get_triggers(self.state)

    def _initial_status(self, status) -> str:
        return status or ElectionStateMachine.INITIAL_STATUS
