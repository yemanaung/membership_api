from typing import Callable, Iterable, List

from membership.database.models import Member, Role
from membership.models import Authorization, MemberAsEligibleToVote, MemberQueryResult
from membership.schemas import JsonObj
from membership.schemas.rest.generic import format_iterable


def format_eligible_member_list(az: Authorization) \
        -> Callable[[Iterable[MemberAsEligibleToVote]], List[JsonObj]]:
    def format_eligible_member(eligible_member: MemberAsEligibleToVote) -> JsonObj:
        email_address_viewable = az.has_role('admin', member_id=eligible_member.id)

        return {
            'id': eligible_member.id,
            'name': eligible_member.name,
            'eligibility': {
                'is_eligible': eligible_member.is_eligible,
                'message': eligible_member.message,
            },
            **({'email': eligible_member.email_address} if email_address_viewable else {})
        }

    return format_iterable(format_eligible_member)


def format_role(role: Role) -> JsonObj:
    return {
        'role': role.role,
        'committee_id': role.committee.id if role.committee else -1,
        'committee_name': role.committee.name if role.committee else 'general',
        # TODO: To be deprecated once the front end no longer depends on 'committee'
        'committee': role.committee.name if role.committee else 'general',
        'date_created': role.date_created
    }


def format_member_query_result(member_query_result: MemberQueryResult, az: Authorization) \
        -> JsonObj:
    return {
        'members': format_eligible_member_list(az)(member_query_result.members),
        'cursor': member_query_result.cursor,
        'has_more': member_query_result.has_more,
    }


def format_member_info(member: Member, az: Authorization) -> JsonObj:
    return {
        'first_name': member.first_name,
        'last_name': member.last_name,
        'biography': member.biography,
        **(
            {'email_address': member.email_address}
            if az.has_role('admin', member_id=member.id)
            else {}
        )
    }


def format_member_basics(member: Member, az: Authorization) -> JsonObj:
    return {
        'id': member.id,
        'info': format_member_info(member, az),
        'roles': [format_role(role) for role in member.roles],
    }


def format_member_full(member: Member, az: Authorization) -> JsonObj:
    if az.member_id == member.id:
        membership = max(member.memberships_usa, key=lambda m: m.dues_paid_until, default=None)
    else:
        membership = None

    return {
        **format_member_basics(member, az),
        'meetings': [attendee.meeting.name for attendee in member.meetings_attended],
        'votes': [
            {
                'election_id': eligible_vote.election_id,
                'election_name': eligible_vote.election.name,
                'election_status': eligible_vote.election.status,
                'voted': eligible_vote.voted,
            } for eligible_vote in member.eligible_votes
        ],
        **(
            {'membership':
                {
                    'address': membership.address,
                    'phone_numbers': [phone_number.number for phone_number in member.phone_numbers],
                    'dues_paid_until': membership.dues_paid_until
                }
             } if membership is not None else {}
        )
    }
