from membership.database.models import Committee
from membership.models import Authorization
from membership.schemas import JsonObj


def format_committee(committee: Committee, az: Authorization) -> JsonObj:
    return {
        'id': committee.id,
        'name': committee.name,
        'viewable': az.has_role('admin') or az.has_role('admin', committee.id)
    }


def format_committee_details(committee: Committee, az: Authorization) -> JsonObj:
    return {
        **(format_committee(committee, az)),
        'admins': [{'id': admin.id, 'name': admin.name} for admin in committee.admins],
        'members': [{'id': member.id, 'name': member.name} for member in committee.members],
        'active': [{'id': member.id, 'name': member.name} for member in committee.active]
    }
