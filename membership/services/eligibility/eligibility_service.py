from typing import List, Union, Optional
from datetime import datetime
from abc import ABC

from membership.database.base import Session
from membership.database.models import Member, Meeting, Attendee
from membership.models import MemberAsEligibleToVote


# This class holds an abstract interface that encapsulates eligibility details.
# Eligibility requirements, semantics, and usage should be encoded within
# subclasses (e.g.; `SanFranciscoEligibilityService`), allowing for flexibility
# with integration in other parts of the application.
class EligibilityService(ABC):
    # Given a list of `Member`s, return a list of delgators that append
    # attributes for eligibility details.
    def members_as_eligible_to_vote(
        self,
        session: Session,
        members: List[Member],
        since: Optional[datetime] = None
    ) -> List[MemberAsEligibleToVote]:
        raise NotImplementedError

    # Given a `Member` and `Meeting` (or an `Attendee`), find the relevant
    # `Attendee` and update any pertinent fields.
    def update_eligibility_to_vote_at_attendance(
        self,
        session: Session,
        member: Optional[Union[Member, int, str]] = None,
        meeting: Optional[Union[Meeting, int, str]] = None,
        attendee: Optional[Union[Attendee, int, str]] = None
    ) -> None:
        raise NotImplementedError
