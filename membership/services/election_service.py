from typing import List, Optional

from membership.database.base import Session
from membership.database.models import Election
from membership.models import ElectionStateMachine


class ElectionService:

    def list_elections(self, session: Session) -> List[Election]:
        return list(session.query(Election).all())

    def find_election_by_id(self, session: Session, election_id: int) -> Optional[Election]:
        return session.query(Election).get(election_id)

    def next_election_transitions(self, election: Election) -> List[str]:
        return ElectionStateMachine(election.status).next_transitions()

    def transition_election_as(self, session: Session, transition: str, election_id: int) \
            -> (bool, Optional[Election]):
        election: Election = self.find_election_by_id(session, election_id)
        if election is None:
            return False, None

        state_machine = ElectionStateMachine(election.status)
        if transition not in state_machine.next_transitions():
            return False, election

        getattr(state_machine, transition)()
        election.status = state_machine.status
        session.merge(election)
        session.commit()

        return True, election
