import pytz
import json
from datetime import datetime
from typing import Any, cast

import sqlalchemy.types as types
from sqlalchemy import create_engine, event, Column
from sqlalchemy.exc import DisconnectionError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from config import settings


def checkout_listener(dbapi_con, con_record, con_proxy):
    """
    Ensures that connections in the pool are still valid before returning them
    :param dbapi_con:
    :param con_record:
    :param con_proxy:
    :return:
    """
    try:
        try:
            dbapi_con.ping(False)
        except TypeError:
            dbapi_con.ping()
    except dbapi_con.OperationalError as exc:
        if exc.args[0] in (2006, 2013, 2014, 2045, 2055):
            raise DisconnectionError()
        else:
            raise


Base = declarative_base()
metadata = Base.metadata
engine = create_engine(**settings)
Session = sessionmaker(bind=engine)
event.listen(engine, 'checkout', checkout_listener)


def col(field: Any) -> Column:
    return cast(Column, field)


def date_parser(date_str: str) -> datetime:
    return datetime.strptime(date_str, '%Y-%m-%d')


class StringyJSON(types.TypeDecorator):
    """Stores and retrieves JSON as TEXT."""

    impl = types.TEXT

    def process_bind_param(self, value, dialect):
        if value is not None:
            value = json.dumps(value)
        return value

    def process_result_value(self, value, dialect):
        if value is not None:
            value = json.loads(value)
        return value


class UTCDateTime(types.TypeDecorator):
    """Make sure all values written to and read from the database as UTC"""

    impl = types.DateTime

    def process_bind_param(self, value, dialect):
        if value is not None and value.tzinfo != pytz.utc:
            value = value.astimezone(pytz.utc)
        return value

    def process_result_value(self, value, dialect):
        if value is not None and hasattr(value, 'tzinfo'):
            if value.tzinfo:
                value = value.astimezone(pytz.utc)
            else:
                value = value.replace(tzinfo=pytz.utc)
        return value


# TypeEngine.with_variant says "use StringyJSON instead when
# connecting to 'sqlite'"
JSON = types.JSON().with_variant(StringyJSON, 'sqlite')
