import pytz

from datetime import datetime


PACIFIC_TIME_ZONE = pytz.timezone('America/Los_Angeles')


def get_current_time() -> datetime:
    return datetime.now(PACIFIC_TIME_ZONE).astimezone(pytz.utc)
