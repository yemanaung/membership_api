"""add_meeting_landing_url_field

Revision ID: cc10730dc34e
Revises: c00a5a9358f1
Create Date: 2018-02-20 04:18:15.003000

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = 'cc10730dc34e'
down_revision = 'c00a5a9358f1'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('meetings', sa.Column('landing_url', sa.String(255), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('meetings', 'landing_url')
    # ### end Alembic commands ###
