"""Pacific timestamps to UTC

Revision ID: 31c54cafa82e
Revises: 63e698688861
Create Date: 2019-03-05 04:51:55.281969

"""

import pytz
from alembic import op
from sqlalchemy.orm import sessionmaker
from membership.database.models import NationalMembershipData, Meeting
from membership.util.time import PACIFIC_TIME_ZONE

# revision identifiers, used by Alembic.
revision = '31c54cafa82e'
down_revision = '63e698688861'
branch_labels = None
depends_on = None


def upgrade():
    """
    For NationalMembershipData and Meeting tables,
    treat their datetime columns as if their data was in Pacific Time
    (and then write them back to the db as UTC)
    """
    bind = op.get_bind()
    session = sessionmaker(bind=bind)()

    def pretend_it_was_pacific(dt):
        if not dt:
            return dt
        return PACIFIC_TIME_ZONE.localize(dt.replace(tzinfo=None)).astimezone(pytz.utc)

    all_natl_memberships = session.query(NationalMembershipData).all()
    for natl_membership in all_natl_memberships:
        natl_membership.join_date = pretend_it_was_pacific(natl_membership.join_date)
        natl_membership.dues_paid_until = pretend_it_was_pacific(natl_membership.dues_paid_until)

    all_meetings = session.query(Meeting).all()
    for meeting in all_meetings:
        meeting.start_time = pretend_it_was_pacific(meeting.start_time)
        meeting.end_time = pretend_it_was_pacific(meeting.end_time)

    session.commit()


def downgrade():
    """
    Reverse the upgrade -- for NationalMembershipData and Meeting tables,
    write their datetime column data back in as "Pacific Time"
    """
    bind = op.get_bind()
    session = sessionmaker(bind=bind)()

    def back_to_pacific(dt):
        if not dt:
            return dt
        return dt.astimezone(PACIFIC_TIME_ZONE).replace(tzinfo=None)

    all_natl_memberships = session.query(NationalMembershipData).all()
    for natl_membership in all_natl_memberships:
        natl_membership.join_date = back_to_pacific(natl_membership.join_date)
        natl_membership.dues_paid_until = back_to_pacific(natl_membership.dues_paid_until)

    all_meetings = session.query(Meeting).all()
    for meeting in all_meetings:
        meeting.start_time = back_to_pacific(meeting.start_time)
        meeting.end_time = back_to_pacific(meeting.end_time)

    session.commit()
