"""one vote per voter

Revision ID: e2dda2f17481
Revises: 6f1ad638bdf1
Create Date: 2017-11-05 10:30:09.629723

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e2dda2f17481'
down_revision = '6f1ad638bdf1'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_index('eligible_voter_member_election_unique', 'eligible_voters',
                    ['member_id', 'election_id'], unique=True)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index('eligible_voter_member_election_unique', table_name='eligible_voters')
    # ### end Alembic commands ###
