"""add description text and image link

Revision ID: d4b670c9f27e
Revises: c576721e1114
Create Date: 2019-02-19 03:50:30.331957

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = 'd4b670c9f27e'
down_revision = 'c576721e1114'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('elections', sa.Column('description', sa.String(length=2048), nullable=True))
    op.add_column('elections', sa.Column('description_img', sa.String(length=1024), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('elections', 'description_img')
    op.drop_column('elections', 'description')
    # ### end Alembic commands ###
