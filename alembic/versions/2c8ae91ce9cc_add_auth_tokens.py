"""add auth tokens

Revision ID: 2c8ae91ce9cc
Revises: adc52c406742
Create Date: 2019-03-12 03:29:52.535091

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '2c8ae91ce9cc'
down_revision = 'adc52c406742'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'auth_token',
        sa.Column('id', sa.String(length=128), nullable=False),
        sa.Column('member_id', sa.Integer(), nullable=False),
        sa.Column('path', sa.String(length=255), nullable=False),
        sa.Column('request_json_params', sa.String(length=255), nullable=True),
        sa.Column('single_use', sa.Boolean(), nullable=False),
        sa.Column('expire_date', sa.DateTime(), nullable=True),
        sa.Column('used', sa.Boolean(), nullable=False),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('id'),
    )


def downgrade():
    op.drop_table('auth_token')
